video novel script
==================

- vnscript is a little script system for setting up video novel style cutscenes (or even complete video novels) for godot
	- execution is one line at a time
	- empty lines are skipped
	- labels are pre-scanned before execution begins and their locations are noted. When execution comes to a label it will just continue right on going as though 'label' were a comment
	- `;;` begins a comment and runs until the end of the line-- there is no way of escaping comments
	- all lines will have leading spaces stripped before execution; use indentation to help legibility
	
	+ dialogend -- ends the dialog scene (wipes self off screen, but don't destruct self)
	
		`dialogend`
	
	+ dumpvars -- print out all the current variables (because godot's debugger doesn't show the contents of dicts)
	
		`dumpvars`
	
	+ log -- print text to console (useful for debugging)
	
		`log something something #{interpthis} something`
	
	+ set var -- sets global var to value. Parsed as json (value must be bool, number, or string)
		setting a variable to NULL will delete it
	
		```
        set { "varname": true }
		set { "varname2": 123.45 }
		set { "varname3": "something something something" }
		```
	
	+ label -- destination for branches and jumps. Alphabetical only. Must not contain spaces
	
		`label labelname`
	
	+ branch -- goes to a label without pushing current location to stack
	
		`branch labelname`
	
	+ jump -- pushes current location to stack then goes to label
	
		`jump labelname`
	
	+ return -- goes back to the previous location and pops off the label stack
	
		`return`
	
	+ wait -- blocks execution of the script for the given number of seconds (handy if you want to wait while an animation and/or audio plays)
	
		`wait 3.5`
		
	+ if statements: if true then the corresponding branch or jump is taken (else the next statement is executed)
		operator must be one of: `is`, `not`, `and`, `or`, `<`, `>`
		operators and their two operands must be surrounded by ()
		nested statements are allowed
		args are checked as literals first: true and false, then checked to see if they're numbers, then check to see if they're string literals
		if not literals then the args are checked to see if they name a global var, if they do then that var value is used
		if not a literal nor a global var then an error is thrown
		
        ```
		if (varname is true) branch labelname
		if ("something" is "foo") branch labelname
		if ((varname > 123) and (varname < 456)) branch labelname
		if ((somevar is "foo") or (othervar is "baz")) branch labelname
		if (((somevar is "foo") and (othervar > 123)) or (anothervar not true)) branch labelname
		
		if (varname is true) jump labelname
		if ("something" is "foo") jump labelname
		if ((varname > 123) and (varname < 456)) jump labelname
		if ((somevar is "foo") or (othervar is "baz")) jump labelname
		if (((somevar is "foo") and (othervar > 123)) or (anothervar not true)) jump labelname
		```
		
		
		```
		expression := '(' <expression> <operator> <expression> ')' | '(' <value> <operator> <expression> ')' | '(' <expression> <operator> <value> ')'
		value := "null" | "true" | "false" | <number> | <identifier> | <string>
		operator := "+" | "-" | "*" | "/" | "is" | "not" | ">" | "<" | "and" | "or"
		number := [0-9] | [0-9].[0-9]
		identifier := [a-z][A-Z][0-9]
		string := """ <characters> """ | """ <interpolation> """ | """ <characters> <interpolation> """ | """ <characters> <interpolation> <characters> """ | """ <interpolation> <characters> """
		interpolation := "#{" <characters> "}"
		characters := zero or more of anything not a quote
		```
		
	- function: the called function can set vars, twiddle inventory, play sound, etc.
		Args are passed as a JSON object and will be given to the called function as a single dict
		Value strings starting with a # name a variable from the global variables list
		the called function needs to have the following signature:
			fname (vnInstance, argumentsDict) -> void
		Notice that functions do not return values. If you want to get something out of a function back into the script
		then set some global vars on the vnInstance from within the function.
		(This scripting language isn't actually intended to be a real programming language!)
		functions block until they return
		
		`call fname { "argname": "something", "anotherarg": 123.45, "somebool": true, "ascriptvar": "#varname" }`
	
	- background -- set the background scene to sceneName
	
		`background sceneName`
	
	- character -- define a character. charid is the identifier for this character (alphabetical only. must not contain spaces)
		a built-in character named NULL can be used if you dont want to show a character portrait or name
	
		`character CHARID { "name": "charname", "scene": "charSceneName" }`
	
	- say -- click-to-advance text. Name and portrait of character corresponding to CHARID will be associated with the text
		Simply @ then the CHARID followed by the text you want to show up (must be on one line in the source file)
		Variables can be interpolated using #{varname} syntax. If the named var does not exist then a warning will be thrown and an empty string interpolated
	
		```
		@CHARID something something something
		@NULL  
		@foo Nice to meet you, #{yourname}
		```
	
	- enter -- show character portrait on the left or right side. First characters in front, newer ones behind
	
	    ```
		enter left CHARID
		enter right CHARID
		```
	
	- leave -- remove character portrait (no need to specify left or right)
	
		`leave CHARID`
	
	- portrait -- sets the character portrait for CHARID to the given scene (replaces it)
	
		`portrait CHARID charSceneName`
	
	- multi-choice -- when choice clicked will branch or jump to corresponding destination label (parsed as json)
		if jumping then return will pick up on the line immediately after the end of the choice
		
		```
		choice branch {
			"display text": "someLabel",
			"another option": "someOtherLabel",
			...
		}
		
		choice jump {
			"display text": "someLabel",
			"another option": "someOtherLabel",
			...
		}
		```
	
	- animate -- animate character portrait (plays once then returns to the way it was before the animation began)
		does not block execution of the script. The script willcontinue while the animation plays
	
		`animate CHARID animationName`
	
	- music -- play an audio track on repeat. does not block
	
		`music audioTrackName`
	
	- sound -- play an audio track once. does not block
	
		`sound audioTrackName`
