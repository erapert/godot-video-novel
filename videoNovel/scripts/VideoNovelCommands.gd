extends Node

var Commands = {
	'null': NullCmd,
	'dialogend': End,
	'log': Log,
	'set': Set,
	'label': LabelCmd,
	'branch': Branch,
	'jump': Jump,
	'return': Return,
	'if': IfCmd,
	'wait': WaitCmd,
	'call': CallFunc,
	'background': Background,
	
	'debugdump': DebugDump
}

class BaseCommand:
	# handle to the vn interpreter through which we access the world
	var interpreter
	# the original string from the .vn file that invokes me
	var originalCmd
	
	func _init (vnInterpreter, cmdStr):
		interpreter = vnInterpreter
		originalCmd = cmdStr
	
	func isValid ():
		return true
		
	func execute ():
		pass
	
	# recurse through the string and insert values named by names
	# given that interpreter.variables = { "foo": "bar", "baz": false }
	# _strInterpolate ("some string with a #{foo} inside it is still #{baz}")
	# returns "some string with a bar inside it is still false"
	func _strInterpolate (interp):
		var beginTag = interp.find ('#{')
		if (beginTag == -1):
			return interp
		var endTag = interp.find ('}', beginTag)
		if (endTag == -1):
			return interp
		var before = interp.left (beginTag)
		var after = _strInterpolate (interp.right (endTag + 1))
		var varName = interp.substr (beginTag + 2, endTag - (beginTag + 2))
		var varValue = "%s" % interpreter.getVariable(varName)
		return "%s%s%s" % [before, varValue, after]


# invalid commands or lines that do nothing
class NullCmd extends BaseCommand:
	func _init (vnInterpreter, cmdStr).(vnInterpreter, cmdStr):
		pass


class End extends BaseCommand:
	func _init (vnInterpreter, cmdStr).(vnInterpreter, cmdStr):
		pass
	
	func isValid ():
		return (originalCmd.nocasecmp_to ('dialogend') == 0)
	
	func execute ():
		interpreter.stop ()


class Log extends BaseCommand:
	var text
	func _init (vnInterpreter, cmdStr).(vnInterpreter, cmdStr):
		# pre-splitting to get the text to print
		# interpolation must be done at execution time
		# in order to capture any variables at run-time not parse-time
		text = originalCmd.substr (originalCmd.findn ('log') + 3, originalCmd.length())
	
	func isValid ():
		return (originalCmd.sub_str (0, 3).nocasecmp_to ('log') == 0)
	
	func execute ():
		print ("VNLOG: %s" % _strInterpolate (text))


class Set extends BaseCommand:
	var rawText
	var allowedTypes = [TYPE_NIL, TYPE_BOOL, TYPE_INT, TYPE_REAL, TYPE_STRING]
	func _init (vnInterpreter, cmdStr).(vnInterpreter, cmdStr):
		rawText = originalCmd.substr (originalCmd.findn ('set') + 3, originalCmd.length())
	
	func isValid ():
		if (originalCmd.sub_str (0, 3).nocasecmp_to ('set') != 0):
			return false
		var testDict = {}
		if (testDict.parse_json (rawText) != OK):
			return false
	
	func execute ():
		var json = JSON.parse (rawText)
		var setDict = {}
		if typeof(json.result) == TYPE_DICTIONARY:
			setDict = json.result
		else:
			print ("Expected JSON object, got %s" % typeof(json.result))
			return

		for key in setDict.keys():
			var val = setDict[key]
			if (allowedTypes.find (typeof (val)) == -1):
				# only null, bool, number, and string are allowed-- no arrays dicts etc.
				setDict.erase (key)
			elif (typeof (val) == TYPE_STRING and val.find ('#{') != -1):
				setDict[key] = _strInterpolate (val)
		interpreter.setVariables (setDict)


class DebugDump extends BaseCommand:
	func _init (vnInterpreter, cmdStr).(vnInterpreter, cmdStr):
		pass
	
	func isValid ():
		return (originalCmd.nocasecmp_to ('debugdump') == 0)
	
	func execute ():
		interpreter.debugDump ()

# labels do nothing, but we still need a placeholder for them so they don't throw off the position of the stack
class LabelCmd extends BaseCommand:
	var labelName = ''
	func _init (vnInterpreter, cmdStr).(vnInterpreter, cmdStr):
		labelName = originalCmd.right ('label'.length() + 1)
		# note: we're counting on the fact that at parse-time commands are created and added to scriptLines
		# as they are encountered in the source file. In this situation we'll be the latest command created
		# and thus my position in scriptLines is the current size of the number of commands
		interpreter.labelPositions [labelName] = interpreter.scriptLines.size()

class Branch extends BaseCommand:
	var labelName = ''
	func _init (vnInterpreter, cmdStr).(vnInterpreter, cmdStr):
		labelName = originalCmd.right ('branch'.length() + 1)
	
	func execute ():
		interpreter.currentLine = interpreter.labelPositions [labelName]

class Jump extends BaseCommand:
	var labelName = ''
	func _init (vnInterpreter, cmdStr).(vnInterpreter, cmdStr):
		labelName = originalCmd.right ('jump'.length() + 1)
	
	func execute ():
		interpreter.jumpStack.push_back (interpreter.currentLine)
		interpreter.currentLine = interpreter.labelPositions [labelName]

class Return extends BaseCommand:
	func _init (vnInterpreter, cmdStr).(vnInterpreter, cmdStr):
		pass
	
	func execute ():
		if (interpreter.jumpStack.size() > 0):
			interpreter.currentLine = interpreter.jumpStack.back ()
			interpreter.jumpStack.pop_back ()
		else:
			print ('WARNING: return was called with no return point on the jump stack. Terminating instead.')
			interpreter.stop ()

# recursively evaluates expression, takes the branch or jump if expression is true
# (have you read your SICP today?)
# expressions look like this: (varname is true), ((something not "something else") and (somevar > 123))
class IfCmd extends BaseCommand:
	var expression = null
	# var branchOrJump = ''
	# var branchLabel = ''
	var jumper = null
	func _init (vnInterpreter, cmdStr).(vnInterpreter, cmdStr):
		var branchPos = originalCmd.find ('branch ')
		var jumpPos = originalCmd.find ('jump ')
		if (branchPos != -1 and jumpPos != -1):
			print ('ERROR: IF statements must be either a branch or a jump, not both.')
			return
		elif (branchPos == -1 and jumpPos == -1):
			print ('ERROR: IF statements must have a branch or jump at the end of the line.')
			return
		elif (branchPos != -1):
			jumper = Branch.new (vnInterpreter, originalCmd.right (branchPos))
			# branchOrJump = 'branch'
			# branchLabel = originalCmd.right (branchPos + 'branch '.length())
		elif (jumpPos != -1):
			jumper = Jump.new (vnInterpreter, originalCmd.right (jumpPos))
			# branchOrJump = 'jump'
			# branchLabel = originalCmd.right (jumpPos + 'jump '.length())
		
		var expressionStart = originalCmd.find ('(')
		var expressionEnd = originalCmd.rfind (')')
		if (expressionStart == -1 or expressionEnd == -1):
			print ('ERROR: IF statements must have an associated expression surrounded by ()')
			return
		
		expression = load ('res://videoNovel/scripts/Expression.gd').new()
		expression.interpreter = vnInterpreter
		expression.parse (originalCmd.substr (expressionStart, (expressionEnd + 1) - expressionStart))
	
	func execute ():
		if expression == null:
			print ('ERROR: null expression')
		
		if expression.evaluate ():
			jumper.execute ()

class WaitCmd extends BaseCommand:
	var duration = 0.0
	
	func _init (vnInterpreter, cmdStr).(vnInterpreter, cmdStr):
		var seconds = originalCmd.right (5) # 'wait ' is 5 chars
		duration = seconds.to_float ()
	
	func execute ():
		interpreter.wait (duration)

class CallFunc extends BaseCommand:
	func _init (vnInterpreter, cmdStr).(vnInterpreter, cmdStr):
		# get func name
		# 
		pass
	
	func execute ():
		pass

class Background extends BaseCommand:
	var sceneName = null
	func _init (vnInterpreter, cmdStr).(vnInterpreter, cmdStr):
		sceneName = originalCmd.right (11) # 'background ' is 11 chars
	
	func execute ():
		interpreter.theatre.background (sceneName)

# this class isn't meant to do things on its own, it's really just a namespace for the above commands
func _ready():
	pass
