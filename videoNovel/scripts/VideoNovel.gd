extends Node2D

#
#	This is the interpreter for .vn files
#	This class is responsible for loading .vn files, parsing them out into commands, and then running them
#	The commands themselves are implemented in VideoNovelCommands.gd
#

export(String) var scriptFilePath


# notice that we must .new() this up so that we can access the list of Commands{}
var Cmds = preload ('res://videoNovel/scripts/VideoNovelCommands.gd').new()

# states of our state machine
enum State {
	HALT,		# stop everything and shutdown
	EXECUTE,	# execute one command at a time
	WAIT		# stop executing but don't reset
}

# the current state of our state machine
# EXECUTE, halt, wait, etc.
var currentState

# characters in the .vn script
# keys are charids, values are VideoNovelCharacter instances
var characters = {}

# variables set in the .vn script
# keys are var names, values are var values
var variables = {}

# positions of labels in the .vn script
# keys are label names, values are integer indexes (line numbers)
var labelPositions = {}

# like a call stack this keeps track of where the last position was so that we can return from jumps
# an array of integer indexes into the script
var jumpStack = []

# the lines in the script ready to be executed
# an array of video novel commands (see VideoNovelCommands.gd)
var scriptLines = []

# the current line for execution
var currentLine = 0

# book keeping for how much time is left to wait
var waitDuration = 0.0

# handle to the thing that does the pretty pictures
var theatre = null

# todo: set up references into the main game
# (refs to player controller or global state or whatever)
func _ready ():
	theatre = get_node ('Theatre')

func reset ():
	characters = {}
	variables = {}
	jumpStack = []
	currentLine = 0
	setState (State.HALT)

func start ():
	reset ()
	setState (State.EXECUTE)
	return self

func stop ():
	# stop everything and init to 0
	setState (State.HALT)
	reset ()
	return self

func pause ():
	# don't change book keeping, simply stop executing and wait to resume
	setState (State.WAIT)

func resume ():
	# go back to executing
	setState (State.EXECUTE)

func setState (newState):
	# todo: if we need to do something fancy or update book keeping do that here
	currentState = newState
	if (currentState == State.HALT):
		set_process (false)
	else:
		set_process (true)

func wait (waitTime):
	waitDuration = waitTime
	setState (State.WAIT)

func _process (delta):
	if (currentLine < 0 or currentLine >= scriptLines.size()):
		print ("ERROR: currentLine (%d) not within bounds of scriptLines (0 ... %d)" % [currentLine, scriptLines.size() - 1])
		setState (State.HALT)
		return
		
	if (currentState == State.HALT):
		return
	
	if (currentState == State.WAIT):
		waitDuration -= delta
		if waitDuration <= 0:
			waitDuration = 0
			setState (State.EXECUTE)
		return
	
	if (currentState == State.EXECUTE):
		scriptLines[currentLine].execute ()
		currentLine += 1

# variables from dict will be inserted into our variables
# non-existing keys will be created and set to their new value
# existing keys will be stomped over with their new value
# setting a key to null will delete it
func setVariables (dict):
	for key in dict:
		if (dict[key] == null):
			variables.erase (key)
		else:
			variables [key] = dict [key]

func getVariable (key):
	if (variables.has (key)):
		return variables[key]
	else:
		print ("WARNING: Tried to access non-existent variable named '%s'" % key)
		return null

# gets the script ready to execute
func loadScript ():
	# load the script file
	var file = File.new ()
	if (!file.file_exists (scriptFilePath)):
		print ("File not found: '%s'" % scriptFilePath)
		return
		
	file.open (scriptFilePath, file.READ)
	if (!file.is_open ()):
		print ("Failed to open script file '%s'" % scriptFilePath)
		return
	else:
		print ("Opened '%s' (%d)" % [scriptFilePath, file.get_len()])
	
	file.seek (0)
	var rawLines = file.get_as_text().split ('\n', true)
	for rawLine in rawLines:
		# stripping leading white space to allow writers to indent without breakage
		# we can also strip out comments here
		var line = stripComment (rawLine.strip_edges (true, false))
		var validCommand = false
		# handling empty lines specially so that the warning later doesn't cry wolf
		if (line == ''):
			scriptLines.push_back (Cmds.Commands['null'].new (self, line))
			validCommand = true
		else:
			for cmd in Cmds.Commands.keys():
				if (line.findn (cmd) == 0):
					scriptLines.push_back (Cmds.Commands[cmd].new (self, line))
					validCommand = true
		if (!validCommand):
			print ('WARNING: invalid command "%s"' % line)
			scriptLines.push_back (Cmds.Commands['null'].new (self, line))

	file.close()
	return self

# comments run from wherever they're found until the end of the line
# if in doubt then put comments on their own lines
func stripComment (input):
	var begin = input.find (';;')
	if (begin == -1):
		return input
	else:
		return input.left (begin)

func debugDump ():
	print ('=======================')
	print ('variables:')
	for key in variables:
		if (typeof (variables[key]) == TYPE_NIL):
			print ("WARNING: %s => null : variables aren't supposed to be null" % key)
		elif (typeof (variables[key]) == TYPE_BOOL):
			print ("\tvariables[%s] => %s" % [key, variables[key]])
		elif (typeof (variables[key]) == TYPE_INT):
			print ("\tWARNING: %s => (int) %s : variables shouldn't be getting parsed as ints (json only supports floats)" % [key, variables[key]])
		elif (typeof (variables[key]) == TYPE_REAL):
			print ("\tvariables[%s] => %s" % [key, variables[key]])
		elif (typeof (variables[key]) == TYPE_STRING):
			print ("\tvariables[%s] => \"%s\"" % [key, variables[key]])
	
	print ('\nlabelPositions:')
	for labelName in labelPositions:
		print ("\t%s : %d" % [labelName, labelPositions[labelName]])
	
	print ('\njump stack:')
	for i in jumpStack:
		print ('\t%d' % i)
	
	print ('\nscript lines (currentLine: %d):' % currentLine)
	var i = 0
	while (i < scriptLines.size()):
		var cmd = scriptLines[i]
		if (i == currentLine):
			print ("==> %s" % cmd.originalCmd)
		else:
			print ("    %s" % cmd.originalCmd)
		i += 1
	print ('=======================')
