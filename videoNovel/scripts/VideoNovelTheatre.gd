extends Node

var definedCharacters = {
	'narrator': { 'name': '', 'scene': 'res://videoNovel/scenes/characters/narrator.tscn' }
}

export(Array, String) var definedBackgrounds = [
	'res://videoNovel/assets/backgrounds/default_black.png'
]

# node instance
var currBackground = null
# charID => node instance
var currCharacters = {}

func background (background_name):
	# set background
	$background
	# return current background
	return $background

# charID = string identifier
# charInfo = { "name": "string", "scene": "res://some/scene" }
func defCharacter (charID, charInfo):
	# characters[charID] = charInfo
	pass

func say (charID, text):
	pass

# direction = 'left' || 'right' || 'none'
func enter (charID, direction):
	# if direction == 'none' then fade in
	pass

# direction = 'left' || 'right' || 'none'
func leave (charID, direction):
	# if direction == 'none' then fade in
	pass

func setCharPortrait (charID, sceneName):
	# todo: keep some kind of stack of char portraits so that you can push and pop?
	pass

func animateCharacter (charID, animationName):
	# delegate animation to character scene--
	#	simply attempt to call the animationName func on the char scene
	pass

func animateBackground (animationName):
	# delegate animation to background scene
	pass

func music (musicResourceName):
	# start playing musicResourceName on a loop
	# return musicResourceName
	pass

func sound (soundResourceName):
	# start playing soundResourceName once
	# return soundResourceName
	pass

func _ready ():
	print('theatre _ready')
