extends Node

enum TokenizeState {
	NULL,
	GATHERING_NUMBER
	GATHERING_IDENTIFIER,
	GATHERING_STRING,
	GATHERING_EXPRESSION,
	DONE
}

enum ValueType {
	NULL,
	BOOL,
	NUMBER,
	STRING,
	IDENTIFIER,
	EXPRESSION
}

enum NodeType {
	NULL,
	VALUE,
	EXPRESSION
}

enum OperatorType {
	oIS, oNOT, oAND, oOR, oGT, oLT, oADD, oSUB, oMUL, oDIV
}
var operators = {
	'is': OperatorType.oIS,
	'not': OperatorType.oNOT,
	'and': OperatorType.oAND,
	'or': OperatorType.oOR,
	'>': OperatorType.oGT,
	'<': OperatorType.oLT,
	'+': OperatorType.oADD,
	'-': OperatorType.oSUB,
	'*': OperatorType.oMUL,
	'/': OperatorType.oDIV
}

# handle to an interperter through which we can lookup variables by name (interpreter.getVariable())
var interpreter = null
var tokens = []
var AST = null

func _makeExpression (valA, op, valB):
	return { 'operator': op, 'A': valA, 'B': valB }

func _makeNode (node_type, value_type, val):
	return { 'node_type': node_type, 'value_type': value_type, 'value': val }

func _makeNullNode ():
	return _makeNode (NodeType.NULL, ValueType.NULL, null)

func _isValueNode (node):
	return node['node_type'] == NodeType.VALUE

func _isOperatorToken (token):
	return operators.has (token)

func _tokenToOperator (token):
	if _isOperatorToken (token):
		return operators[token]
	else:
		print ('ERROR: not a valid operator: "%s"' % token)
		return null

func _isExpressionNode (node):
	return (node['node_type'] == NodeType.EXPRESSION &&
		node['value']['operator'] != null &&
		node['value']['A'] != null &&
		node['value']['B'] != null)
	

# sets AST to a nested dict (evaluate it with EvaluateExpression) like:
# input a possibly nested expression string like: (true is true)
# {
#	'node_type': NodeType.EXPRESSION,
#	'value_type': ValueType.EXPRESSION,
#	'operator': OperatorType.oIS,
#	'A': {
#		'node_type': NodeType.VALUE,
#		'value_type': ValueType.BOOL,
#		'value': true
#	},
#	'B': {
#		'node_type': NodeType.VALUE,
#		'value_type': ValueType.BOOL,
#		'value': true
#	},
# }
func parse (exprStr):
	tokens = tokenize (exprStr)
	if tokens[0] != '(' or tokens[tokens.size() - 1] != ')':
		print ('ERROR: expressions must be enclosed in parenthesis: ( and )')
		return false
	#print_ast_tokens (tokens)
	AST = _getValue ()
	return _isExpressionNode (AST)
				


# read and return an expression node from the tokens list
# [ '(', 'value', 'operator', 'value', ')' ] => { 'node_type': NodeType.EXPRESSION, 'value_type': ValueType.EXPRESSION, 'value': { 'operator': op, 'A': valA, 'B': valB } }
func _getExpression ():
	var operator = null
	var A = null
	var B = null
	var token = null
	
	A = _getValue ()
	
	token = _popToken ()
	operator = _tokenToOperator (token)
	if operator == null:
		print ('ERROR: expected operator but found "%s"' % token)
		_dbgPrintDict (A)
	
	B = _getValue ()
	
	if operator != null && A != null && B != null:
		return _makeNode (NodeType.EXPRESSION, ValueType.EXPRESSION, _makeExpression (A, operator, B))
	else:
		return _makeNullNode ()

# read and return the next value from the tokens list
# '123' => { 'node_type': NodeType.VALUE, 'value_type': ValueType.NUMBER, 'value': 123 }
func _getValue ():
	var rtrn = _makeNullNode ()
	var token = _popToken ()
	if token == '(':
		rtrn = _getExpression ()
		if !_isExpressionNode (rtrn):
			print ('ERROR: expected an expression but found:')
			_dbgPrintDict (rtrn)
		if tokens.size() > 0:
			if tokens[0] == ')':
				_popToken ()
			else:
				print ('ERROR: expected end of expression but found "%s"' %tokens[0])
		else:
			print ('ERROR: expected end of expression but found nothing instead: are you missing a ")"?')
	else:
		rtrn = tokenToValue (token)
		if !_isValueNode (rtrn):
			print ('ERROR: expected a value or expression but found:')
			_dbgPrintDict (rtrn)
	return rtrn



func _popToken ():
	if tokens.size() > 0:
		var rtrn = tokens[0]
		tokens.pop_front()
		return rtrn
	else:
		return null
		
		

func tokenToValue (token):
	var nType = NodeType.VALUE
	var vType = ValueType.NULL
	var val = null
	
	if token == null:
		print ('ERROR: cannot convert null token to AST node: %s' % token)
		return _makeNullNode ()

	if token.find('"') == 0:
		# slice off the enclosing quotes and convert escaped quotes to regular quotes
		var strVal = token.substr(1, token.length() - 2).replace("'\"", '"')
		vType = ValueType.STRING
		val = strVal
	
	elif token.is_valid_float():
		vType = ValueType.NUMBER
		val = token.to_float()
	
	# operators aren't values
	#elif operators.find(token) != -1:
	#	vType = ValueType.OPERATOR
	#	val = token
	
	elif token.to_lower() == 'true':
		vType = ValueType.BOOL
		val = true
	
	elif token.to_lower() == 'false':
		vType = ValueType.BOOL
		val = false
	
	elif token.to_lower() == 'null':
		vType = ValueType.NULL
		val = null
	
	elif token.is_valid_identifier():
		vType = ValueType.IDENTIFIER
		val = token
	
	else:
		print ("INVALID TOKEN: %s" % token)
		return _makeNullNode ()
	
	return _makeNode (nType, vType, val)


func print_ast_tokens (tokens):
	var indent = ''
	for token in tokens:
		if token == '(':
			print ('%sBEGIN expression' % [indent])
			indent += "\t"
		elif token == ')':
			indent = indent.left (indent.length() - 1)
			print ('%sEND expression' % [indent])
		elif token.find('"') == 0:
			# slice off the enclosing quotes and convert escaped quotes to regular quotes
			var val = token.substr(1, token.length() - 2).replace("'\"", '"')
			print ("%sSTRING: %s" % [indent, val])
		elif token.is_valid_float():
			print ("%sNUMBER: %s" % [indent, token])
		elif operators.find(token) != -1:
			print ("%sOPERATOR: %s" % [indent, token])
		elif token.to_lower() == 'true' or token.to_lower() == 'false':
			print ("%sBOOLEAN: %s" % [indent, token])
		elif token.to_lower() == 'null':
			print ("%sNULL: %s" % [indent, token])
		elif token.is_valid_identifier():
			print ("%sIDENTIFIER: %s" % [indent, token])
		else:
			print ("%sINVALID TOKEN: %s" % [indent, token])


# input: expression string
# returns an array of tokenized strings:
# '("foo" == true)' => [ '(', '"foo"', '==', 'true', ')' ]
func tokenize (exprStr):
	#print ('_getToken ("%s")' % exprStr)
	var tokenState = TokenizeState.NULL
	var gatheredTokens = []
	var tokenSoFar = ''
	
	for c in exprStr:
		#print ('c: %s' % c)
		if tokenState == TokenizeState.NULL:
			if c == ' ' or c == '\t':
				pass
			elif c == '(' or c == ')':
				gatheredTokens.push_back ("%s" % c)
				tokenSoFar = ''
			else:
				tokenSoFar += c
				if c == '"':
					tokenState = TokenizeState.GATHERING_STRING
				elif c == '-' or c == '+' or c.is_valid_integer():
					tokenState = TokenizeState.GATHERING_NUMBER
				else:
					tokenState = TokenizeState.GATHERING_IDENTIFIER
		
		#
		#	strings are delimited with ""
		#	use ' to escape a quote
		#	examples: "one two three", "it's easy", "here's a '"quote'" within a quote"
		#	strings can include interpolations: "this var: #{somevar} will be interpolated when the expression is evaluated"
		#
		elif tokenState == TokenizeState.GATHERING_STRING:
			tokenSoFar += c
			# double quote not preceeded by a single quote is an end quote
			if c == '"' and tokenSoFar.rfind("'") != tokenSoFar.length() - 2:
				#print ('STRING: %s' % tokenSoFar)
				gatheredTokens.push_back (tokenSoFar)
				tokenSoFar = ''
				tokenState = TokenizeState.NULL
		
		#
		#	numbers are of these forms: 123 , +123 , -123 , 0.123 , +1.23 , -1.23
		#	numbers must always start with a sign or a digit; this is an error: .123
		#
		elif tokenState == TokenizeState.GATHERING_NUMBER:
			if c == ' ' or c == '\t' or c == '(' or c == ')':
				if !tokenSoFar.is_valid_float():
					print ('ERROR: Invalid number: %s' % tokenSoFar)
					return null
				else:
					gatheredTokens.push_back (tokenSoFar)
					if c == '(' or c == ')':
						gatheredTokens.push_back ("%s" % c)
					tokenSoFar = ''
					tokenState = TokenizeState.NULL
			else:
				tokenSoFar += c
		
		
		#
		#	identifiers are: null, true, false, word_with_no_spaces, <some operator>
		#
		elif tokenState == TokenizeState.GATHERING_IDENTIFIER:
			# any of these characters tell us we're at the end of the identifier
			if c == ' ' or c == '\t' or c == '(' or c == ')':
				# gdscript supplies .is_valid_identifier() for strings
				if tokenSoFar.is_valid_identifier() or _isOperatorToken (tokenSoFar):
					gatheredTokens.push_back (tokenSoFar)
					if c == '(' or c == ')':
						gatheredTokens.push_back ("%s" % c)
					
					tokenSoFar = ''
					tokenState = TokenizeState.NULL
					
				else:
					print ('ERROR: Invalid identifier: %s' % tokenSoFar)
					return null
			else:
				tokenSoFar += c
		
		#
		#
		#
		else:
			print ('ERROR: Invalid tokenize state (%d) (%s) (%s)' % [tokenState, tokenSoFar, c])
			return null
	
	return gatheredTokens
	
func debugPrint ():
	print (AST)

func _dbgPrintDict (d):
	for key in d.keys():
		print ("%s => %s" % [key, d[key]])

func evaluate ():
	return evalExpr (AST)

func evalExpr (expr):
	#print ('eval expr:')
	#_dbgPrintDict (expr)
	
	var aval = null
	var bval = null
	var op = expr['value']['operator']
	
	var anode = expr['value']['A']
	if anode['node_type'] == NodeType.EXPRESSION:
		aval = evalExpr (anode)
	elif anode['node_type'] == NodeType.VALUE:
		aval = evalVal (anode)
	else:
		aval = null
	
	var bnode = expr['value']['B']
	if bnode['node_type'] == NodeType.EXPRESSION:
		bval = evalExpr (bnode)
	elif anode['node_type'] == NodeType.VALUE:
		bval = evalVal (bnode)
	else:
		bval = null
	return evalOp (op, aval, bval)

func evalVal (val):
	#print ('eval val:')
	#_dbgPrintDict (val)
	var vtype = val['value_type']
	if vtype == ValueType.IDENTIFIER:
		return interpreter.getVariable (val['value'])
	elif vtype == ValueType.EXPRESSION:
		return evalExpr (val)
	else:
		return val['value']

# oIS, oNOT, oAND, oOR, oGT, oLT, oADD, oSUB, oMUL, oDIV
func evalOp (op, a, b):
	if op == OperatorType.oIS:
		return a == b
	elif op == OperatorType.oNOT:
		return a != b
	elif op == OperatorType.oAND:
		return a && b
	elif op == OperatorType.oOR:
		return a || b
	elif op == OperatorType.oGT:
		return a > b
	elif op == OperatorType.oLT:
		return a < b
	else:
		print ('unimplemented operator: %s' % op)

func _ready ():
	pass
